/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjetoUnicornios;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author conta2
 */
public class TelaAcaoController implements Initializable {
    
    @FXML
    private Label mensagemResposta;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void gerenciaUni(ActionEvent event) {
        if(TipoUnicornio.verificaExisteBanco2()){
        Parent root;
        try {

            Stage stage = MainDoProjeto.stage;
            
            root = FXMLLoader.load(getClass().getResource("TelaGerenciaUnicornios.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
        } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
        } 
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Não Existe Nenhum Tipo");
        }
    }

    @FXML
    private void gerenciaTipoUni(ActionEvent event) {
        Parent root;
        try {

            Stage stage = MainDoProjeto.stage;
            
            root = FXMLLoader.load(getClass().getResource("TelaGerenciadorTipo.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
        } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
        } 
    }

    @FXML
    private void gerenciaGrupos(ActionEvent event) {
        Parent root;
            try {

            Stage stage = MainDoProjeto.stage;
            
            root = FXMLLoader.load(getClass().getResource("TelaGerenciaGrupos.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
            } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
            }
    }
    
    @FXML
    private void login(ActionEvent event) {
        Parent root;
            try {

            Stage stage = MainDoProjeto.stage;
            
            root = FXMLLoader.load(getClass().getResource("TelaLogin.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
            } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
            }
    }
    
}
