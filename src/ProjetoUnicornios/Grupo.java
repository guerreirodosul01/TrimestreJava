/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjetoUnicornios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author conta2
 */
public class Grupo {
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_GRUPOSUNICORNIO (NOME,USERUNI ) VALUES (?,?)";
        

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, this.nome);
            
            preparedStatement.setString(2, MainDoProjeto.dadosGerais.getUserLogado());
          
            
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta();
        }        
        return true;
    }
    
    public static ArrayList<Grupo> getAll() {
        String selectSQL = "SELECT * FROM OO_GRUPOSUNICORNIO WHERE USERUNI = ?";
        ArrayList<Grupo> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setString(1, MainDoProjeto.dadosGerais.getUserLogado());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Grupo f = new Grupo();
                f.setNome(rs.getString("NOME"));
                
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public static ArrayList<Unicornio> getAllMembros(String grupo) {
        String selectSQL = "SELECT * FROM OO_UNICORNIO WHERE OO_UNICORNIO.rg  IN (SELECT UNICORNIO FROM OO_MEMBROSGRUPOSUNICORNIO WHERE USERUNI = ?  AND GRUPO = ?)  AND USERUNI = ? ";
        ArrayList<Unicornio> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setString(1, MainDoProjeto.dadosGerais.getUserLogado());
            st.setString(3, MainDoProjeto.dadosGerais.getUserLogado());
            st.setString(2, grupo);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Unicornio f = new Unicornio();
                f.setNome(rs.getString("NOME"));
                f.setRg(rs.getInt("RG"));
                f.setAltura(rs.getInt("ALTURA"));
                f.setTipo(rs.getString("TIPO"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    public static ArrayList<Unicornio> getAllNaoMembros(String grupo) {
        String selectSQL = "SELECT * FROM OO_UNICORNIO WHERE OO_UNICORNIO.rg NOT IN (SELECT UNICORNIO FROM OO_MEMBROSGRUPOSUNICORNIO WHERE USERUNI = ? AND GRUPO = ?)  AND USERUNI = ? ";
        ArrayList<Unicornio> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setString(1, MainDoProjeto.dadosGerais.getUserLogado());
            st.setString(3, MainDoProjeto.dadosGerais.getUserLogado());
            st.setString(2, grupo);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Unicornio f = new Unicornio();
                f.setNome(rs.getString("NOME"));
                f.setRg(rs.getInt("RG"));
                f.setAltura(rs.getInt("ALTURA"));
                f.setTipo(rs.getString("TIPO"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public boolean getOne(String nome) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT * FROM OO_GRUPOSUNICORNIO WHERE NOME = ? and USERUNI = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nome);
            ps.setString(2, MainDoProjeto.dadosGerais.getUserLogado());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
               
                this.setNome(rs.getString("NOME"));
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); //Boa pratica
        }         
        return true;
    }
    
    
    
    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM OO_GRUPOSUNICORNIO WHERE NOME = ? AND USERUNI = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.nome);
            ps.setString(2, MainDoProjeto.dadosGerais.getUserLogado());
          
            deleteMembros();
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
        
    }
    
    public static boolean verificaExisteBanco(String nome) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT * FROM OO_GRUPOSUNICORNIO WHERE NOME = ? AND USERUNI  = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nome);// 
            ps.setString(2, MainDoProjeto.dadosGerais.getUserLogado());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return true;
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }         
        return false;
    }
    
    public boolean insertMembro(int membro) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_MEMBROSGRUPOSUNICORNIO (GRUPO,USERUNI,UNICORNIO ) VALUES (?,?,?)";
        

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, this.nome);
            
            preparedStatement.setString(2, MainDoProjeto.dadosGerais.getUserLogado());
            preparedStatement.setInt(3, membro);
          
            
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta();
        }        
        return true;
    }
    
    public boolean deleteMembro(int membro) {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM OO_MEMBROSGRUPOSUNICORNIO WHERE GRUPO = ? AND USERUNI = ? AND UNICORNIO = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.nome);
            ps.setString(2, MainDoProjeto.dadosGerais.getUserLogado());
            ps.setInt(3, membro);
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
        
    }
    
    public boolean deleteMembros() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM OO_MEMBROSGRUPOSUNICORNIO WHERE GRUPO = ? AND USERUNI = ? ";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.nome);
            ps.setString(2, MainDoProjeto.dadosGerais.getUserLogado());
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
        
    }
    
}
