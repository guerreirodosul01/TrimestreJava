/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjetoUnicornios;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author conta2
 */
public class TelaGerenciaUnicornioController implements Initializable {

    @FXML
    private Label mensagemResposta;
    @FXML
    private Button excluirButton;
    @FXML
    private Button atualizarButton;
    @FXML
    private TableView<Unicornio> tabela;
    @FXML
    private TableColumn<Unicornio, Integer> rgCol;
    @FXML
    private TableColumn<Unicornio, String> nomeCol;
    @FXML
    private TableColumn<Unicornio, Integer> alturaCol;
    @FXML
    private TableColumn<Unicornio, String> tipoCol;
    @FXML
    private Button adicionaUnicornioButton;
    @FXML
    private TextField altura;
    @FXML
    private TextField nome;
    @FXML
    private TextField rg;
    @FXML
    private ChoiceBox<String> tipos;
    
    private ObservableList<Unicornio> unicornioList;
    private ObservableList<String> tiposList;

    private Unicornio unicornio;
    
    

    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        atualiza();
        excluirButton.setDisable(true);
        atualizarButton.setDisable(true);
        tiposList = tipos.getItems();
        TipoUnicornio.getAll().forEach((a) -> {
            tiposList.add(a.getNomeTipo());
        });
        
        
    }    

    @FXML
    private void voltaPaginaInicial(ActionEvent event) {
        Parent root;
        try {

            Stage stage = MainDoProjeto.stage;
            
            root = FXMLLoader.load(getClass().getResource("TelaAcao.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
        } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
        } 
        
    }

    @FXML
    private void excluiItem(ActionEvent event) {
        Unicornio unicornioSelecionado = tabela.getSelectionModel().getSelectedItem();
        boolean resposta = unicornioSelecionado.delete();
        unicornioList.remove(unicornioSelecionado);
        limpaInputs(event);
        
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Dado Foi Excluido Corretamente");
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Excluir");
        }
    }

    @FXML
    private void atualizaDados(ActionEvent event) {
        
        if(!nome.getText().isEmpty() && !altura.getText().isEmpty() && tipos.getSelectionModel().getSelectedItem() != null){
            if(altura.getText().matches("^[0-9]*[.]{0,1}[0-9]*$")){
        tabela.getSelectionModel().getSelectedItem().setNome(nome.getText());
        tabela.getSelectionModel().getSelectedItem().setAltura(Double.valueOf(altura.getText()));
        tabela.getSelectionModel().getSelectedItem().setTipo(tipos.getSelectionModel().getSelectedItem());
        boolean resposta = tabela.getSelectionModel().getSelectedItem().update();
        atualiza();
        limpaInputs(event);
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Dado Foi Atualizado Corretamente");
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Atualizar");
        }
            }else{
                mensagemResposta.setTextFill(Color.RED);
                mensagemResposta.setText("Use Numeros Na Altura");
            }
        }else{
           mensagemResposta.setTextFill(Color.RED);
           mensagemResposta.setText("Não Deixe Nada Em Braco"); 
            
        }
    }

    @FXML
    private void limpaInputs(ActionEvent event) {
        tabela.getSelectionModel().clearSelection();
        nome.setText("");
        rg.setDisable(false);
        rg.setText("");
        altura.setText("");
        tipos.getSelectionModel().clearSelection();
        
        excluirButton.setDisable(true);
        atualizarButton.setDisable(true);
        adicionaUnicornioButton.setDisable(false);
        
        mensagemResposta.setText("");
    }

    @FXML
    private void selecionado(MouseEvent event) {
        if(tabela.getSelectionModel().getSelectedItem() != null){
            excluirButton.setDisable(false);
            atualizarButton.setDisable(false);
            adicionaUnicornioButton.setDisable(true);
            
            
            Unicornio unicornioSelecionado = tabela.getSelectionModel().getSelectedItem();
            nome.setText(unicornioSelecionado.getNome());
            rg.setDisable(true);
            rg.setText(String.valueOf(unicornioSelecionado.getRg()));
            altura.setText(String.valueOf(unicornioSelecionado.getAltura()));
            tipos.setValue(unicornioSelecionado.getTipo());
        }
    }

    @FXML
    private void adicionaTipo(ActionEvent event) {
        cadastrar();
    }
    
    public void atualiza(){
        
        unicornioList = tabela.getItems();
        unicornioList.clear();
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        rgCol.setCellValueFactory(new PropertyValueFactory<>("rg")); 
        alturaCol.setCellValueFactory(new PropertyValueFactory<>("altura"));
        tipoCol.setCellValueFactory(new PropertyValueFactory<>("tipo")); 
        Unicornio.getAll().forEach((a) -> {
            unicornioList.add(a);
        });
        
    }
    public void cadastrar(){
        if(!nome.getText().isEmpty() && !rg.getText().isEmpty() && !altura.getText().isEmpty() && tipos.getSelectionModel().getSelectedItem()!= null ){
            if(altura.getText().matches("^[0-9]*[.]{0,1}[0-9]*$") && rg.getText().matches("^[0-9]*[.]{0,1}[0-9]*$")){
            if(!Unicornio.verificaExisteBanco(Integer.valueOf(rg.getText()))){
                   unicornio = new Unicornio();
                   unicornio.setNome(nome.getText());
                   unicornio.setTipo(tipos.getSelectionModel().getSelectedItem());
                   unicornio.setRg(Integer.valueOf(rg.getText()));
                   unicornio.setAltura(Double.valueOf(altura.getText()));
                   boolean resposta = unicornio.insert();

                   
                   
                   if(resposta){
                        mensagemResposta.setTextFill(Color.GREEN);
                        mensagemResposta.setText("Dado Foi Cadastrado Corretamente");
                    }else{
                        mensagemResposta.setTextFill(Color.RED);
                        mensagemResposta.setText("Erro Ao Cadastrar");
                    }
                   
                   nome.setText("");
                   rg.setText("");
                   altura.setText("");
                   tipos.getSelectionModel().clearSelection();
                   
                   
                   unicornioList.add(unicornio);
            }else{
                mensagemResposta.setTextFill(Color.RED);
                mensagemResposta.setText("Esse RG Já Está Sendo Usado");
            }
            }else{
                 mensagemResposta.setTextFill(Color.RED);
                 mensagemResposta.setText("Use Numeros Na Altura E No RG");
            }
        }else{
           mensagemResposta.setTextFill(Color.RED);
           mensagemResposta.setText("Não Deixe Nada Em Braco"); 
            
        }



        }
    
}
