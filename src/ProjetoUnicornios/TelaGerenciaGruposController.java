/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjetoUnicornios;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author conta2
 */
public class TelaGerenciaGruposController implements Initializable {

    @FXML
    private TableView<Grupo> grupoTable;
    @FXML
    private TableColumn<Grupo, String> nomeGrupoCol;
    @FXML
    private TableView<Unicornio> naoMembrosTable;
    @FXML
    private TableColumn<Unicornio, String> rgNaoMembroCol;
    @FXML
    private TableColumn<Unicornio, String> nomeNaoMembroCol;
    @FXML
    private TableColumn<Unicornio, String> alturaNaoMembroCol;
    @FXML
    private TableColumn<Unicornio, String> tipoNaoMembroCol;
    @FXML
    private TableView<Unicornio> membrosTable;
    @FXML
    private TableColumn<Unicornio, String> rgMembrosCol;
    @FXML
    private TableColumn<Unicornio, String> nomeMembrosCol;
    @FXML
    private TableColumn<Unicornio, String> alturaMembrosCol;
    @FXML
    private TableColumn<Unicornio, String> tipoMembrosCol;
    @FXML
    private TextField nome;
    @FXML
    private Button cadastrarButton;
    @FXML
    private Label mensagemResposta;
    @FXML
    private Button excluirButton;
    @FXML
    private Button adiconarAoGrupoButton;
    @FXML
    private Button removerButton;
    
    private ObservableList<Grupo> grupoList;
    private ObservableList<Unicornio> membrosList;
    private ObservableList<Unicornio> naoMembrosList;

    
    
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        atualiza();
        excluirButton.setDisable(true);
        adiconarAoGrupoButton.setDisable(true);
        removerButton.setDisable(true);
    }  
    
    public void atualiza(){
        
        grupoList = grupoTable.getItems();
        grupoList.clear();
        nomeGrupoCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        
        Grupo.getAll().forEach((a) -> {
            grupoList.add(a);
        });
        
        
    }
    
    public void atualizaMembros(){
        membrosList = membrosTable.getItems();
        membrosList.clear();
        nomeNaoMembroCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        rgMembrosCol.setCellValueFactory(new PropertyValueFactory<>("rg")); 
        alturaNaoMembroCol.setCellValueFactory(new PropertyValueFactory<>("altura"));
        tipoNaoMembroCol.setCellValueFactory(new PropertyValueFactory<>("tipo"));      
        Grupo.getAllMembros(grupoTable.getSelectionModel().getSelectedItem().getNome()).forEach((a) -> {
            membrosList.add(a);
        });
        
        naoMembrosList = naoMembrosTable.getItems();
        naoMembrosList.clear();
        nomeNaoMembroCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        rgNaoMembroCol.setCellValueFactory(new PropertyValueFactory<>("rg")); 
        alturaMembrosCol.setCellValueFactory(new PropertyValueFactory<>("altura"));
        tipoMembrosCol.setCellValueFactory(new PropertyValueFactory<>("tipo"));      
        Grupo.getAllNaoMembros(grupoTable.getSelectionModel().getSelectedItem().getNome()).forEach((a) -> {
            naoMembrosList.add(a);
        });

        
    }

    @FXML
    private void grupoSelecionado(MouseEvent event) {
        atualizaMembros();
        excluirButton.setDisable(false);
    }

    @FXML
    private void naoMembrosSelecionado(MouseEvent event) {
        adiconarAoGrupoButton.setDisable(false);
    }

    @FXML
    private void membrosSelecionado(MouseEvent event) {
        removerButton.setDisable(false);
    }

    @FXML
    private void cadastrar(ActionEvent event) {
        if(!nome.getText().isEmpty()){
            if(Grupo.verificaExisteBanco(nome.getText())){
                mensagemResposta.setTextFill(Color.RED);
                mensagemResposta.setText("Grupo Já Em Uso");
            }else{
            Grupo grupo = new Grupo();
            grupo.setNome(nome.getText());
            
            boolean resulta = grupo.insert();
            if(resulta){
                mensagemResposta.setTextFill(Color.GREEN);
                mensagemResposta.setText("Cadastrado Com Sucesso");
            }else{
                mensagemResposta.setTextFill(Color.RED);
                mensagemResposta.setText("Erro Ao Cadastrar");
            }
            }
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Preencha Coretamente");
        }
        atualiza();
    }

    @FXML
    private void excluirGrupo(ActionEvent event) {
    
        membrosTable.getSelectionModel().clearSelection();
        naoMembrosTable.getSelectionModel().clearSelection();
        membrosList.clear();
        naoMembrosList.clear();
        excluirButton.setDisable(true);
        adiconarAoGrupoButton.setDisable(true);
        removerButton.setDisable(true);
        boolean resposta = grupoTable.getSelectionModel().getSelectedItem().delete();
        grupoList.remove(grupoTable.getSelectionModel().getSelectedItem());
        grupoTable.getSelectionModel().clearSelection();
        
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Grupo E Membros Foram Excluidos Corretamente");
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Excluir");
        }
        
        atualiza();
    }

    @FXML
    private void adiconarAoGrupo(ActionEvent event) {
        
        
        boolean resposta = grupoTable.getSelectionModel().getSelectedItem().insertMembro(naoMembrosTable.getSelectionModel().getSelectedItem().getRg());
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Dado Foi Adicionado Corretamente");
            membrosTable.getSelectionModel().clearSelection();
            naoMembrosTable.getSelectionModel().clearSelection();
            adiconarAoGrupoButton.setDisable(true);
            removerButton.setDisable(true);
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Adicionar");
        }
        atualizaMembros();
    }

    @FXML
    private void removerMembro(ActionEvent event) {
        
        
        
        boolean resposta = grupoTable.getSelectionModel().getSelectedItem().deleteMembro(membrosTable.getSelectionModel().getSelectedItem().getRg());
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Dado Foi Removido Corretamente");
            membrosTable.getSelectionModel().clearSelection();
            naoMembrosTable.getSelectionModel().clearSelection();
            adiconarAoGrupoButton.setDisable(true);
            removerButton.setDisable(true);
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Remover");
        }
        atualizaMembros();
    }

    @FXML
    private void voltarInicio(ActionEvent event) {
        Parent root;
        try {

            Stage stage = MainDoProjeto.stage;
            
            root = FXMLLoader.load(getClass().getResource("TelaAcao.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
        } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
        } 
    }
    
}
