/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjetoUnicornios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author conta2
 */
public class Usuario {
    private String nome,senha;

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_USUARIOUNICORNIO (NOME, SENHA ) VALUES (?,?)";
        

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, this.nome);
            preparedStatement.setString(2, this.senha);
          
            
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta();
        }        
        return true;
    }
    
    public static ArrayList<Usuario> getAll() {
        String selectSQL = "SELECT * FROM OO_USUARIOUNICORNIO";
        ArrayList<Usuario> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                Usuario f = new Usuario();
                f.setNome(rs.getString("NOME"));
                f.setSenha(rs.getString("SENHA"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public boolean getOne(String nome) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT * FROM OO_USUARIOUNICORNIO WHERE NOME = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nome);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
               
                this.setNome(rs.getString("NOME"));
               
                this.setSenha(rs.getString("SENHA"));
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); //Boa pratica
        }         
        return true;
    }
    
    public boolean update() {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE OO_USUARIOUNICORNIO SET SENHA =? WHERE NOME = ?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.senha);
            
            ps.setString(2, this.nome);
            
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM OO_USUARIOUNICORNIO WHERE NOME = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.nome);
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
        
    }
    
    public static boolean verificaExisteBanco(String nome) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT * FROM OO_USUARIOUNICORNIO WHERE NOME = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nome);// 
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return true;
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }         
        return false;
    }
    
    public static boolean verificaExisteBanco2(String nome, String senha) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT * FROM OO_USUARIOUNICORNIO WHERE NOME = ? AND SENHA = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nome);// 
            ps.setString(2, senha);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return true;
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }         
        return false;
    }
    
}
