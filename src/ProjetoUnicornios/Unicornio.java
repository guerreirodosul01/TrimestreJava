/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjetoUnicornios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author conta2
 */
public class Unicornio{
    private String nome,tipo;
    private int rg;
    private double altura;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getRg() {
        return rg;
    }

    public void setRg(int rg) {
        this.rg = rg;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_UNICORNIO (NOME, RG , ALTURA , TIPO,USERUNI ) VALUES (?,?,?,?,?)";
        

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, this.nome);
            preparedStatement.setInt(2, this.rg);
            preparedStatement.setDouble(3,this.altura);
            preparedStatement.setString(4, this.tipo);
            preparedStatement.setString(5, MainDoProjeto.dadosGerais.getUserLogado());
          
            
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta();
        }        
        return true;
    }
    
    public static ArrayList<Unicornio> getAll() {
        String selectSQL = "SELECT * FROM OO_UNICORNIO WHERE USERUNI = ?";
        ArrayList<Unicornio> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setString(1, MainDoProjeto.dadosGerais.getUserLogado());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Unicornio f = new Unicornio();
                f.setNome(rs.getString("NOME"));
                f.setRg(rs.getInt("RG"));
                f.setAltura(rs.getInt("ALTURA"));
                f.setTipo(rs.getString("TIPO"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public boolean getOne(int rg) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT * FROM OO_UNICORNIO WHERE RG = ? and USERUNI = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, rg);
            ps.setString(2, MainDoProjeto.dadosGerais.getUserLogado());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                this.setRg(rs.getInt("RG"));
                this.setNome(rs.getString("NOME"));
                this.setAltura(rs.getInt("ALTURA"));
                this.setTipo(rs.getString("TIPO"));
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); //Boa pratica
        }         
        return true;
    }
    
    public boolean update() {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE OO_UNICORNIO SET NOME =?, ALTURA= ?, TIPO=? WHERE RG = ? and USERUNI = ?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.nome);
            ps.setDouble(2, this.altura);
            ps.setString(3, this.tipo);
            ps.setInt(4, this.rg);
            ps.setString(5, MainDoProjeto.dadosGerais.getUserLogado());
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM OO_UNICORNIO WHERE RG = ? AND USERUNI = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, this.rg);
            ps.setString(2, MainDoProjeto.dadosGerais.getUserLogado());
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
        
    }
    
    public static boolean verificaExisteBanco(int rg) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT * FROM OO_UNICORNIO WHERE RG = ? AND USERUNI  = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, rg);// 
            ps.setString(2, MainDoProjeto.dadosGerais.getUserLogado());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return true;
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }         
        return false;
    }
}
