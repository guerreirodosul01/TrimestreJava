/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjetoUnicornios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author conta2
 */
public class TipoUnicornio  {
    private String nomeTipo,cor,origem,material;

    public String getNomeTipo() {
        return nomeTipo;
    }

    public void setNomeTipo(String nomeTipo) {
        this.nomeTipo = nomeTipo;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OO_TIPOUNICORNIO (NOME, COR , ORIGEM , MATERAL, USERUNI ) VALUES (?,?,?,?,?)";
        

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, this.nomeTipo);
            preparedStatement.setString(2, this.cor);
            preparedStatement.setString(3,this.origem);
            preparedStatement.setString(4, this.material);
            preparedStatement.setString(5, MainDoProjeto.dadosGerais.getUserLogado());
          
            
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
           
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    public static ArrayList<TipoUnicornio> getAll() {
        String selectSQL = "SELECT * FROM OO_TIPOUNICORNIO WHERE USERUNI = ?";
        ArrayList<TipoUnicornio> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setString(1, MainDoProjeto.dadosGerais.getUserLogado());
            ResultSet rs = st.executeQuery();
            
            while (rs.next()) {
                TipoUnicornio f = new TipoUnicornio();
                f.setNomeTipo(rs.getString("NOME"));
                f.setCor(rs.getString("COR"));
                f.setOrigem(rs.getString("ORIGEM"));
                f.setMaterial(rs.getString("MATERAL"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta(); 
        }   

        return lista;
    }
    
    public boolean getOne(String nomeTipo) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT * FROM OO_TIPOUNICORNIO WHERE NOME = ? and USERUNI = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nomeTipo); 
            ps.setString(2, MainDoProjeto.dadosGerais.getUserLogado());
            ResultSet rs = ps.executeQuery(); 
            if(rs.next()){
                this.setNomeTipo(rs.getString("NOME"));
                this.setCor(rs.getString("COR"));
                this.setOrigem(rs.getString("ORIGEM"));
                this.setMaterial(rs.getString("MATERAL"));
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta();
        }         
        return true;
    }
    
    public boolean update() {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE OO_TIPOUNICORNIO SET COR =?, ORIGEM= ?, MATERAL=? WHERE NOME = ? and USERUNI = ?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.cor);
            ps.setString(2, this.origem);
            ps.setString(3, this.material);
            ps.setString(4, this.nomeTipo);
            ps.setString(5, MainDoProjeto.dadosGerais.getUserLogado());
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM OO_TIPOUNICORNIO WHERE NOME = ? AND USERUNI = ?";        
        
        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.nomeTipo);
            ps.setString(2, MainDoProjeto.dadosGerais.getUserLogado());
          
            deleteUnicornios();
            ps.executeUpdate();
        } catch (SQLException e) {
           
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }      
        
        return true;
        
    }
    public boolean deleteUnicornios() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM OO_UNICORNIO WHERE TIPO = ? AND USERUNI  = ? ";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.nomeTipo);
            ps.setString(2, MainDoProjeto.dadosGerais.getUserLogado());
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
           
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
        
    }
    
    public static boolean verificaExisteBanco(String nomeTipo) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT * FROM OO_TIPOUNICORNIO WHERE NOME = ? AND USERUNI = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nomeTipo); 
            ps.setString(2, MainDoProjeto.dadosGerais.getUserLogado());
            ResultSet rs = ps.executeQuery(); 
            if(rs.next()){
                return true;
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }         
        return false;
    }
    
    public static boolean verificaExisteBanco2() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        

        String selectSQL = "SELECT * FROM OO_TIPOUNICORNIO WHERE USERUNI = ?";
        PreparedStatement st;
        try { 
            st = dbConnection.prepareStatement(selectSQL);
            st.setString(1, MainDoProjeto.dadosGerais.getUserLogado());
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                return true;
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }         
        return false;
    }
    
    
    
}
