package ProjetoUnicornios;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class GerenciadorDeTiposController implements Initializable {

    @FXML
    private Button adicionaTipoButton;

    @FXML
    private TableColumn<TipoUnicornio, String> nomeCol;

    @FXML
    private TextField cor;

    @FXML
    private TextField meterial;

    @FXML
    private Button excluirButton;

    @FXML
    private TextField origem;

    @FXML
    private TextField nome;

    @FXML
    private Button atualizarButton;

    @FXML
    private TableColumn<TipoUnicornio, String> materialCol;

    @FXML
    private TableView<TipoUnicornio> tabela;

    @FXML
    private TableColumn<TipoUnicornio, String> origemCol;

    @FXML
    private TableColumn<TipoUnicornio, String> corCol;
    
    private ObservableList<TipoUnicornio> tipoUnicornioList;

    
    
    private TipoUnicornio tipo;

    @FXML
    private Label mensagemResposta;

    @FXML
    void adicionaTipo(ActionEvent event) {
        cadastrar();
    }

    @FXML
    void limpaInputs(ActionEvent event) {
        tabela.getSelectionModel().clearSelection();
        nome.setText("");
        nome.setDisable(false);
        cor.setText("");
        origem.setText("");
        meterial.setText("");
        excluirButton.setDisable(true);
        atualizarButton.setDisable(true);
        adicionaTipoButton.setDisable(false);
        
        mensagemResposta.setText("");
        
    }

    @FXML
    void atualizaDados(ActionEvent event) {
        
        if(!cor.getText().isEmpty() && !origem.getText().isEmpty() && !meterial.getText().isEmpty()){
        tabela.getSelectionModel().getSelectedItem().setCor(cor.getText());
        tabela.getSelectionModel().getSelectedItem().setOrigem(origem.getText());
        tabela.getSelectionModel().getSelectedItem().setMaterial(meterial.getText());
        boolean resposta = tabela.getSelectionModel().getSelectedItem().update();
        atualiza();
        limpaInputs(event);
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Dado Foi Atualizado Corretamente");
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Atualizar");
        }
        }else{
           mensagemResposta.setTextFill(Color.RED);
           mensagemResposta.setText("Não Deixe Nada Em Braco"); 
            
        }

    }

    @FXML
    void excluiItem(ActionEvent event) {
        TipoUnicornio tipoSelecionado = tabela.getSelectionModel().getSelectedItem();
        boolean resposta = tipoSelecionado.delete();
        tipoUnicornioList.remove(tipoSelecionado);
        limpaInputs(event);
        
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Tipo e Unicornios Com Esse Tipo Foram Excluidos Corretamente");
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Excluir");
        }

    }

    @FXML
    void voltaPaginaInicial(ActionEvent event){
        Parent root;
        try {

            Stage stage = MainDoProjeto.stage;
            
            root = FXMLLoader.load(getClass().getResource("TelaAcao.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
        } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
        } 

    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        

          atualiza();
          excluirButton.setDisable(true);
          atualizarButton.setDisable(true);
          
    

    }
    
    public void atualiza(){
        
        tipoUnicornioList = tabela.getItems();
        tipoUnicornioList.clear();
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nomeTipo"));      
        corCol.setCellValueFactory(new PropertyValueFactory<>("cor")); 
        origemCol.setCellValueFactory(new PropertyValueFactory<>("origem"));
        materialCol.setCellValueFactory(new PropertyValueFactory<>("material")); 
        TipoUnicornio.getAll().forEach((a) -> {
            tipoUnicornioList.add(a);
        });
        
    }
    
    public void cadastrar(){
        if(!nome.getText().isEmpty() && !cor.getText().isEmpty() && !origem.getText().isEmpty() && !meterial.getText().isEmpty()){
            if(!TipoUnicornio.verificaExisteBanco(nome.getText())){
                   tipo = new TipoUnicornio();
                   tipo.setNomeTipo(nome.getText());
                   tipo.setCor(cor.getText());
                   tipo.setOrigem(origem.getText());
                   tipo.setMaterial(meterial.getText());
                   boolean resposta = tipo.insert();

                   
                   
                   if(resposta){
                        mensagemResposta.setTextFill(Color.GREEN);
                        mensagemResposta.setText("Dado Foi Cadastrado Corretamente");
                    }else{
                        mensagemResposta.setTextFill(Color.RED);
                        mensagemResposta.setText("Erro Ao Cadastrar");
                    }
                   
                   nome.setText("");
                   cor.setText("");
                   origem.setText("");
                   meterial.setText("");
                   
                   
                   tipoUnicornioList.add(tipo);
            }else{
                mensagemResposta.setTextFill(Color.RED);
                mensagemResposta.setText("Esse NOME Já Está Sendo Usado");
            }
        }else{
           mensagemResposta.setTextFill(Color.RED);
           mensagemResposta.setText("Não Deixe Nada Em Braco"); 
            
        }



        }
    
    public void selecionado(){
        
        
        if(tabela.getSelectionModel().getSelectedItem() != null){
            excluirButton.setDisable(false);
            atualizarButton.setDisable(false);
            adicionaTipoButton.setDisable(true);
            
            
            TipoUnicornio tipoSelecionado = tabela.getSelectionModel().getSelectedItem();
            nome.setText(tipoSelecionado.getNomeTipo());
            nome.setDisable(true);
            cor.setText(tipoSelecionado.getCor());
            origem.setText(tipoSelecionado.getOrigem());
            meterial.setText(tipoSelecionado.getMaterial());
        }
    }
    
    

}
